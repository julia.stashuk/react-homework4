
import { Routes, Route } from 'react-router-dom';
import Cart from './Routes/CartPage/Cart';
import Favorite from './Routes/FavoritePage/Favorite';
import Home from './Routes/HomePage/Home';


const AppRouter = ({ products, cart, incrementCartItem, decrementCartItem, handleFavourite, handleAddToCart, handleRemoveFromCart }) => {
    return (
        <Routes>
        <Route path='/' element={<Home products={products} 
      handleFavourite={handleFavourite} 
      handleAddToCart={handleAddToCart}
      handleRemoveFromCart={handleRemoveFromCart}/>} />
        <Route path='cart' element={<Cart products={products} cart={cart} 
        decrementCartItem={decrementCartItem}
        incrementCartItem={incrementCartItem}
        handleRemoveFromCart={handleRemoveFromCart}/>}/>
        <Route path='favorite' element={<Favorite handleAddToCart={handleAddToCart}
        products={products} handleFavourite={handleFavourite}
        />}/>
        </Routes>
    )
}

export default AppRouter;
