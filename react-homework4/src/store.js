import { configureStore } from '@reduxjs/toolkit';
import productsReducer from './redux/productsSlice';
import modalReducer from './redux/modalSlice';

const store = configureStore({
    reducer: {
        products: productsReducer,
        modal: modalReducer
    },
});


export default store;