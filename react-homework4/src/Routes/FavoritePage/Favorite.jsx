import PropTypes from 'prop-types';
import CardContainer from '../../Component/CardContainer/CardContainer'
import React, { useState, useEffect } from 'react';

const Favorite = ({ products = [], handleFavourite = () => { },
handleAddToCart = () => { }, }) => {

    const [favoriteProducts, setFavoriteProducts] = useState([]);

    useEffect(() => {
        // Отримання обраних товарів з localStorage при завантаженні компонента
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        // Фільтрація продуктів, щоб відобразити тільки обрані
        const filteredProducts = products.filter(product => favorites.includes(product.id));
        setFavoriteProducts(filteredProducts);
    }, [products]);


    return (
        <>
            <h1>Selected</h1>

            <CardContainer products={favoriteProducts}
                handleFavourite={handleFavourite}
                handleAddToCart={handleAddToCart}

            />
        </>
    )
}



Favorite.propTypes = {
    products: PropTypes.array,
    handleFavourite: PropTypes.func,

};
export default Favorite;
