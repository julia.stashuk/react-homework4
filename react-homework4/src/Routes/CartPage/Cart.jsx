import React from 'react';
import CartItem from '../../Component/CartItem/CartItem'
import './Cart.scss'
import { useSelector } from 'react-redux';

const Cart = ({ decrementCartItem = () => { }, incrementCartItem = () => { }, handleRemoveFromCart = () => { } }) => {
    
    const products = useSelector((state) => state.products.items);
    const cart = useSelector((state) => state.products.cart);
    
    const cartProducts = products.filter(item => cart.some(cartItem => cartItem.id === item.id));
    const totalPrice = cart.reduce((acc, cartItem) => {
        const product = products.find(item => item.id === cartItem.id);
        if (product) {
            
            return acc + product.price * cartItem.quantity;
        } else {
            return acc; 
        }
    }, 0);
    
    const roundedTotalPrice = totalPrice.toFixed(2);
    
       return (
           <>
               <h1>Cart</h1>
               <div className="container">
                   {cartProducts.map(item => <CartItem
                       key={item.id}
                       item={item}
                       cart={cart}
                       products={products}
                       decrementCartItem={decrementCartItem}
                       incrementCartItem={incrementCartItem}
                       handleRemoveFromCart={handleRemoveFromCart}
                   />)}
                   
              </div>
               <h2> TOTAL PRICE: {roundedTotalPrice}</h2>
   
           </>
       )
   }


export default Cart