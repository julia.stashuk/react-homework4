
import React from 'react'
import "./Button.scss"
import PropTypes from 'prop-types'


const Button = ({ type, className, backgroundColor,text, onClick = () => {},  }) => {
    
    return (
        <button type={type} className={className} style = {{backgroundColor}} onClick={onClick} >
            {text}
        </button>
    );
}; 
Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,  
}
export default Button;
 
