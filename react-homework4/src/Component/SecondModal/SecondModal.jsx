import React from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import PropTypes from 'prop-types'; 

const SecondModal = ({ handleCloseModal, handleAddToCart, title, price, id}) => {
  
  const handleAddToCartClick = () => {
    handleAddToCart(id);
    handleCloseModal();
};
  return (
    <Modal
    handleCloseModal={handleCloseModal}
    modalType="second"
    backgroundColor="rgba(213, 212, 194, 1)"
    closeButton={true}
    showImage={false}
    header={`Add Product "${title}"`}
    text={<span className="modal-text">{price}</span>}
    actions={ 
    <Button
      className="main-button"
      backgroundColor="rgba(151, 137, 12, 0.475)"
      text="ADD TO CART"
      onClick={handleAddToCartClick}
    />} 
  />   
  );
}

SecondModal.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired
};

export default SecondModal;



  
  