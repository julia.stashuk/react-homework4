import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openModal, closeModal } from '../../redux/modalSlice';
import { incrementCartItem, decrementCartItem, removeFromCart } from '../../redux/productsSlice';
import FirstModal from '../FirstModal/FirstModal';
import './CartItem.scss'

function CartItem({
    item = {},
    products = [],
}) {

    const dispatch = useDispatch();
    const isModalOpen = useSelector((state) => state.modal.isOpen);
    const modalType = useSelector((state) => state.modal.modalType);
    const cart = useSelector((state) => state.products.cart);

    const cartItem = cart.find(cartItem => cartItem.id === item.id);
    const quantityInCart = cartItem ? cartItem.quantity : 0;

    const product = products.find(product => product.id === item.id);
    const totalPrice = product ? product.price * quantityInCart : 0;
    const roundedTotalPrice = totalPrice.toFixed(2);

    const handleFirstModalOpen = () => {
      if (!isModalOpen) {
      dispatch(openModal("first"));
      }
  };
 
    return (
        <div className="item">
          <div className='item-content'>
            <img className="product-image"  src={item.image} alt={item.title} />
            <h2 className="product-name">{item.title}</h2>
            <p className="product-price">Price: {item.price}</p>
          
          <div className='item-footer'>
            <button className="main-button count-button" onClick={() => { dispatch(decrementCartItem({ id: item.id })) }}>-</button>
            <p className="quantity">{quantityInCart}</p>
            <button className="main-button count-button" onClick={() => { dispatch(incrementCartItem({ id: item.id })) }}>+</button>
          </div>
          <p className="count">Total: {roundedTotalPrice}</p>
          <div className='item-close'>
            <button className="close-button" onClick={handleFirstModalOpen}>X</button>
          </div>
          </div>
          
          {isModalOpen && (
                <FirstModal
                    handleCloseFirstModal={() => dispatch(closeModal())}
                    item={item}
                    title={item.title}
                    image={item.image}
                    handleRemoveFromCart={() => dispatch(removeFromCart({ id: item.id }))}
                />)}
        </div>
    );
}

CartItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
    quantity: PropTypes.number,
  }),
  
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string,
      image: PropTypes.string,
      price: PropTypes.number,
    })
  )
};
export default CartItem;
